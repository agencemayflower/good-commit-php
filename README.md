# good-commit-php

To do cz-conventional-commit for php

Getting Started
-
Please run:

    # cd PATH/TO/GIT_ROOT
    git clone git@bitbucket.org:agencemayflower/good-commit-php.git 
    cp -p good-commit-php/package.json . 
    cp -p good-commit-php/.precise-phplint .
    cp -p good-commit-php/.precise-prettier-php . 
    
Automation
-
**Composer** can automatically run `npm install` after his installation,
 
To *enable* add this to your **composer.json**:

    "scripts": {
      ...
      "post-install-cmd": [
        "[ $COMPOSER_DEV_MODE -eq 0 ] || npm install # IF is DEV_MODE run npm install"
      ]
    },
      
**NPM** can automatically run child `npm install` after his installation, 

To enable add this to your **package.json**:

    "scripts": {
      ...
      "postinstall": "cd YOUR_CHILD_PATH && npm install"
    },
